<?php

$extsAllowed = array('png');

$ext_normal_upload = strtolower(substr(strrchr($_FILES["normal"]["name"], '.'), 1));
$ext_student_upload = strtolower(substr(strrchr($_FILES["student"]["name"], '.'), 1));

if (in_array($ext_normal_upload, $extsAllowed) && in_array($ext_student_upload, $extsAllowed)) {
    list($width, $height) = getimagesize($_FILES["normal"]["name"]);
    $image = imagecreatefrompng($_FILES["normal"]["name"]);

    list($student_width, $student_height) = getimagesize($_FILES["student"]["name"]);
    $student_image = imagecreatefrompng($_FILES["student"]["name"]);

    function grey_image()
    {
        global $width, $height, $image;
        $grey_image = imagecreatetruecolor($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);

                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                $result_rgb = 0.3 * $r + 0.59 * $g + 0.11 * $b;
                $result_rgb = intval($result_rgb);
                $r = $g = $b = $result_rgb;

                $color = imagecolorallocate($grey_image, $r, $g, $b);
                $color = intval($color);

                imagesetpixel($grey_image, $x, $y, $color);
            }
        }
        imagepng($grey_image, 'grey.png');
        return $grey_image;
    }

    function negative_image()
    {
        global $width, $height, $image;
        $negative_image = imagecreatetruecolor($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);

                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                $negative_r = 255 - $r;
                $negative_g = 255 - $g;
                $negative_b = 255 - $b;

                $color = imagecolorallocate($negative_image, $negative_r, $negative_g, $negative_b);
                $color = intval($color);

                imagesetpixel($negative_image, $x, $y, $color);
            }
        }
        imagepng($negative_image, 'negative.png');
        return $negative_image;
    }

    function median_filter($grey_image)
    {
        global $width, $height;
        $median_filter_image = imagecreatetruecolor($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {

                $rgb_array[0] = imagecolorat($grey_image, $x - 1, $y - 1);
                $rgb_array[1] = imagecolorat($grey_image, $x - 1, $y);
                $rgb_array[2] = imagecolorat($grey_image, $x - 1, $y + 1);
                $rgb_array[3] = imagecolorat($grey_image, $x, $y - 1);
                $rgb_array[4] = imagecolorat($grey_image, $x, $y);
                $rgb_array[5] = imagecolorat($grey_image, $x, $y + 1);
                $rgb_array[6] = imagecolorat($grey_image, $x + 1, $y - 1);
                $rgb_array[7] = imagecolorat($grey_image, $x + 1, $y);
                $rgb_array[8] = imagecolorat($grey_image, $x + 1, $y + 1);

                sort($rgb_array);

                $rgb = $rgb_array[4];

                if (isset($rgb)) {
                    $r = $g = $b = $rgb & 0xFF;
                } else {
                    $r = $g = $b = 0;
                }

                $color = imagecolorallocate($median_filter_image, $r, $g, $b);

                imagesetpixel($median_filter_image, $x, $y, $color);
            }
        }
        imagepng($median_filter_image, 'median.png');
        return $median_filter_image;
    }

    function harmonic_means($grey_image)
    {
        global $width, $height;
        $harmonic_means_image = imagecreatetruecolor($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $sum = 0;
                $count = 0;
                $rgb_array[0] = imagecolorat($grey_image, $x - 1, $y - 1);
                $rgb_array[1] = imagecolorat($grey_image, $x - 1, $y);
                $rgb_array[2] = imagecolorat($grey_image, $x - 1, $y + 1);
                $rgb_array[3] = imagecolorat($grey_image, $x, $y - 1);
                $rgb_array[4] = imagecolorat($grey_image, $x, $y);
                $rgb_array[5] = imagecolorat($grey_image, $x, $y + 1);
                $rgb_array[6] = imagecolorat($grey_image, $x + 1, $y - 1);
                $rgb_array[7] = imagecolorat($grey_image, $x + 1, $y);
                $rgb_array[8] = imagecolorat($grey_image, $x + 1, $y + 1);

                foreach ($rgb_array as $rgb) {
                    $rgb_color = $rgb & 0xFF;
                    if ($rgb_color == 0) {
                        continue;
                    }
                    $sum += 1 / $rgb_color;
                    $count++;
                }
                $r = $g = $b = $count / $sum;
                $pixel_color = imagecolorallocate($harmonic_means_image, $r, $g, $b);
                imagesetpixel($harmonic_means_image, $x, $y, $pixel_color);
            }
        }
        imagepng($harmonic_means_image, 'harmonic.png');
        return $harmonic_means_image;
    }

    function student_filter($bright)
    {
        global $student_width, $student_height, $student_image;
        $width = $student_width;
        $height = $student_height;
        $image = $student_image;
        $student_filter_image = imagecreatetruecolor($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);

                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                if ($r + $bright < 255) {
                    $r += $bright;
                } else {
                    $r = 255;
                }

                if ($g + $bright < 255) {
                    $g += $bright;
                } else {
                    $g = 255;
                }

                if ($b + $bright < 255) {
                    $b += $bright;
                } else {
                    $b = 255;
                }

                $color = imagecolorallocate($student_filter_image, $r, $g, $b);
                $color = intval($color);

                imagesetpixel($student_filter_image, $x, $y, $color);
            }
        }
        imagepng($student_filter_image, 'student_filter.png');
        return $student_filter_image;
    }

    function generate_histogram($histogram, $message)
    {
        $max_height = 300;
        $bar_width = 2;
        $max = 0;
        for ($i = 0; $i < 255; $i++) {
            if ($histogram[$i] > $max) {
                $max = $histogram[$i];
            }
        }
        echo "<h1>$message</h1>";
        echo "<div style='width: " . (256 * $bar_width) . "px; border: 1px solid'>";
        for ($i = 0; $i < 255; $i++) {
            $height = ($histogram[$i] / $max) * $max_height;
            echo "<img style=\"background-color: black;\" width=\"" . $bar_width . "\" height=\"" . $height . "\" border=\"0\">";
        }
        echo "</div>";
    }

    function show_histogram($image, $message)
    {
        global $width, $height;
        $histogram = array();
        $size = $width * $height;
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);

                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                $bright = 0.3 * $r + 0.59 * $g + 0.11 * $b;
                $bright = intval($bright);

                $histogram[$bright] += $bright / $size;
            }
        }
        generate_histogram($histogram, $message);
    }

    function show_normal_image()
    {
        if (file_exists($_FILES["normal"]["name"])) {
            $normal_image = $_FILES["normal"]["name"];
            echo "<img src=$normal_image>";
        }
    }

    function show_grey_image()
    {
        if (file_exists('grey.png')) {
            echo "<img src='grey.png'>";
        }
    }

    function show_negative_image()
    {
        if (file_exists('negative.png')) {
            echo "<img src='negative.png'>";
        }
    }

    function show_median_filter_image()
    {
        if (file_exists('median.png')) {
            echo "<img src='median.png'>";
        }
    }

    function show_harmonic_means_image()
    {
        if (file_exists('harmonic.png')) {
            echo "<img src='harmonic.png'>";
        }
    }

    function show_student_image()
    {
        if (file_exists('student_filter.png')) {
            echo "<img src='student_filter.png'>";
        }
    }

    $normal_image = $image;
    $grey_image = grey_image();
    $negative_image = negative_image();
    $median_filter_image = median_filter($grey_image);
    $student_image = student_filter(85);
    $harmonic_means_image = harmonic_means($grey_image);

    show_histogram($normal_image, "Нормальное изображение");
    show_normal_image();

    show_histogram($grey_image, "ЧБ");
    show_grey_image();

    show_histogram($negative_image, "Изображение в негативе");
    show_negative_image();

    show_histogram($median_filter_image, "Изображение с использованием медианного фильтра");
    show_median_filter_image();

    show_histogram($harmonic_means_image, "Изображение с использованием фильтра «гармоническое среднее»");
    show_harmonic_means_image();

    show_histogram($student_image, "Изображение студента с повышением яркости");
    show_student_image();
} else {
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'index.php';
    header("Location: http://$host$uri/$extra");
}